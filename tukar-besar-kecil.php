<?php
function tukar_besar_kecil($string){
	$array = str_split($string);
	$convert = "";
	foreach ($array as $key => $value) {
		if(ctype_upper($value)){
			$convert .= strtolower($value);
		}else{
			$convert .= strtoupper($value);
		}
	}
	$convert .= "<br>";

	return $convert;

}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>