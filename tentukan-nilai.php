<?php
function tentukan_nilai($number)
{
	$string = "";
    if($number >= 85 && $number <= 100){
    	$string = "Sangat baik <br>";
    }else if($number >= 70 && $number < 85){
    	$string = "Baik <br>";
    }else if($number >= 60 && $number < 70){
    	$string = "Cukup <br>";
    }else{
    	$string = "Kurang <br>";
    }

    return $string;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>