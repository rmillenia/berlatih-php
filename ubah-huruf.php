<?php
function ubah_huruf($string){
	$char = range('a', 'z');
	$lower = strtolower($string);
	$split = str_split($lower);
	$hasil = '';
	
	foreach ($split as $key => $value) {
		if( in_array($value ,$char))
		{
			$index = array_search($value ,$char);
			if($index == 25){
				$hasil .= $char[0];
			}else{
				$hasil .= $char[$index+1];
			}
		}
	}

	$hasil .= "<br>";

	return $hasil;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>